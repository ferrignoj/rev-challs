#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char valid_license[] = "75ecd8bc-8327-455b-925c-9928b5cfeecd";

void check_license(const char * license){
	if(strlen(license) == 0){
		fprintf(stderr, "Invalid license len\n");
		exit(-1);
	}
	if(strlen(license) != strlen(valid_license)){
		fprintf(stderr, "license have not the same len\n");
		exit(-1);
	}
	if(strncpy(valid_license, license, strlen(valid_license))){
		fprintf(stdout, "Urra it is a valid code!\n");
	}else{
		fprintf(stderr, "Too bad they do not match\n");
		exit(-1);
	}
}


int main(int argc, char const **argv)
{
	if(argc < 2){
		fprintf(stderr, "Usage: %s <license>\n", argv[0]);
		exit(-1);
	}
	check_license(argv[1]);
	return 0;
}
