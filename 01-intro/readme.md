# Objective

You have to find the valid license for the binary!

# Subjects
- Elf file structure
- Strings in files

# Tools
strings, ida, hexedit, readelf