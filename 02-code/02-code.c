#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// A valid code is fd5e-4efb-bc7d
int license_len = 14;
int license_sum = 1151;

int char_sum(const char * license){
	int sum=0;
	for( int i=0; i < strlen(license); i++){
		sum += license[i];
	}
	return sum;
}

void check_license(const char * license){
	int string_value = char_sum(license);

#ifdef DEBUG
	printf("The string has len %d and value %d\n", strlen(license), string_value);
#endif

	if(strlen(license) == 0){
		fprintf(stderr, "Invalid license len\n");
		exit(-1);
	}
	if(strlen(license) != license_len){
		fprintf(stderr, "license have not the same len\n");
		exit(-1);
	}

	if(license_sum == string_value){
		fprintf(stdout, "Urra it is a valid code!\n");
	}else{
		fprintf(stderr, "Too bad they do not match\n");
		exit(-1);
	}
}


int main(int argc, char const **argv)
{
	if(argc < 2){
		fprintf(stderr, "Usage: %s <license>\n", argv[0]);
		exit(-1);
	}
	check_license(argv[1]);
	return 0;
}
