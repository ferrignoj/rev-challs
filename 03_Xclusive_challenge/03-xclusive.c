#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define uint8_t char


uint8_t enc_license[] = {0x39,0x36,0x60,0x36,0x35,0x36,0x60,0x3f,0x25,0x3e,0x38,0x38,0x6d,0x20,0x3a,0x3c,0x28,0x21,0x3f,0x2a,0x75,0x73,0x2f,0x3a,0x21,0x7d,0x29,0x7f,0x2f,0x7f,0x7d,0x2b,0x43,0x14,0x12,0x10};

#ifdef DEBUG
void showbits( unsigned int x )
{
    int i=0;
    for (i = (sizeof(int) * 8) - 1; i >= 0; i--)
    {
       putchar(x & (1u << i) ? '1' : '0');
    }
    printf("\n");
}
#endif

int xor(int in, int key){

	int res = 0;
	/*
	Pseudocode for the xor operation
	for(int i=0; i < 0x20; i++){
		int curr_in_bit = ( in >> i ) & 0x1;
		int curr_key_bit = ( key >> i ) & 0x1;

		if(curr_in_bit != curr_key_bit){
			res = res | ( 1 << i);
		}
	} */

	__asm__ volatile (
		"mov $0x0, %%cl;"
		"movl $0x0, %%eax;"
	".loop_xor:"
		"xor %%ebx, %%ebx;"
		"movl %1, %%ebx;"
		"shr %%cl, %%ebx;"
		"and $0x1, %%ebx;"
		"push %%ebx;"
		"xor %%ebx, %%ebx;"
		"movl %2, %%ebx;"
		"shr %%cl, %%ebx;"
		"and $0x1, %%ebx;"
		"push %%eax;"
		"mov 0x4(%%esp), %%eax;"
		"cmp %%ebx, %%eax;"
		"pop %%eax;"
		"pop %%ebx;"
		"jz .do_not_xor;"
	".do_xor:"
		"movl $0x1, %%ebx;"
		"shl %%cl, %%ebx;"
		"or %%ebx, %%eax;"
	".do_not_xor:"
		"inc %%cl;"
		"cmp $0x20, %%cl;"
		"jnz .loop_xor;"
	".end_xor:"
		"movl %%eax, %0;"
		: "=r"(res)
		: "r"(in), "r"(key)
		: "%eax", "%ecx", "%ebx"
		);

#ifdef DEBUG
	showbits(res);
#endif
	return res;
}

char * char_xor_inplace(char * str, int size){
	int finished = 0;
	for(int i=0; finished != 1; i++){
		/* lets reimplement equals in the worse way possible
		The pseudocode is:
		def equal(first, second):
			for i from 0 up to 32:
				b1 = (fist >> i) & 0x1
				b2 = (second >> i) & 0x1
				if b2 != b1:
					return 0
			return 1
		*/
		__asm__ volatile (
				"movl $0x0, %%ecx;"
			".loop:"
				"movl %1, %%eax;"
				"movl %2, %%edx;"
				"shr %%cl, %%eax;"
				"and $0x1, %%eax;"
				"shr %%cl, %%edx;"
				"and $0x1, %%edx;"
				"cmp %%edx, %%eax;"
				"jnz .ne;"
				"inc %%ecx;"
				"cmpl $0x20, %%ecx;"
				"jnz .loop;"
				"movl $0x1, %%eax;"
				"jmp .end;"
			".ne:"
				"movl $0x0, %%eax;"
			".end:"
				"movl %%eax, %0;"
				: "=r"(finished)
				: "r"(i), "r"(size - 1)
				: "%eax", "%edx", "%ecx"
				);

		str[i] = xor(str[i],  i);
#ifdef DEBUG
		printf("OP: %c, finished: %d, i: %d\n", str[i], finished, i);
#endif
	}
	return str;
}

void check_license(char * license){
	char* clean_license = char_xor_inplace(enc_license, sizeof(enc_license));

#ifdef DEBUG
	printf("\n{");
	for(int i=0; i < strlen(clean_license); i++){
		printf("0x%x,", clean_license[i]);
	}
	printf("}\n");
#endif

	if(strlen(license) != strlen(clean_license)){
		exit(-1);
	}
	for(int i=0; i < strlen(clean_license); i++){
		int status = 0;
		__asm__ volatile(
		      "xor %%eax, %%eax;"
		      "xor %%edx, %%edx;"
			  "movzb %1, %%eax;"
		      "movzb %2, %%edx;"
			  "xor %%edx, %%eax;"
			  "movl %%eax, %0"
			  : "=r"(status)
			  : "r"(clean_license[i]), "r"(license[i])
			  : "%eax", "%edx"

		);
#ifdef DEBUG
		printf("OP1: %c, OP2: %c, Status: %d\n", clean_license[i],
												 license[i],
												 status);
#endif

		if(status != 0){
			fprintf(stderr, "Too bad they do not match\n");
			exit(-1);
		}
	}

	fprintf(stdout, "Urra it is a valid code!\n");
}


int main(int argc, char **argv)
{
	if(argc < 2){
		exit(-1);
	}
	check_license(argv[1]);
	return 0;
}
