#include <argp.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// RSA
#include "dep1.h"
// Base64
#include "dep2.h"

/*
[Done] Generate a shuffle order for the char of the key
[Done] Generate a keypair
encrypt the expected order
Throw away the primes and store only the modulus

On normal run:
- take the primes
- make the key
- decrypt the order
- check to see if the order is equals to the one decrypted
- Shuffle the list and return flag

*/

/*  ARGS PARSING*/
const char *argp_program_version = "lets link us v0.0000001";
const char *argp_program_bug_address = "</dev/null>";
static char doc[] = "TODO :)";
static char args_doc[] = "ARG1";
static struct argp_option options[] = {
    {"generate", 'g', 0, 0,  "Generate a keypair" },
    {"generate_shuffle", 's', 0, 0, "Generate shuffle"},
    {"encrypt", 'c', 0, 0, "Encrypt a message"},
    {"pr", 'p',0,0},
    {"es", 'e', 0, 0, ""},
    {"input", 'i', "inpt", 0, "input form user" },
    { 0 }
};

struct arguments {
  char *args[1];
  int generate;
  int generate_from_primes;
  int generate_shuffle;
  int encrypt;
  int shuffle_and_encrypt;
  char *input;
};

static error_t parse_opt (int key, char *arg, struct argp_state *state) {
  struct arguments *arguments = state->input;

  switch (key)
    {
    case 'g':
        arguments->generate = 1;
        break;
    case 'p':
        arguments->generate_from_primes = 1;
    case 'i':
        arguments->input = arg;
        break;
    case 's':
        // Yep no depenencies to the input, expect SIGSEV :)
        arguments->generate_shuffle = 1;
        break;
    case 'c':
        arguments->encrypt = 1;
        break;
    case 'e':
        arguments->shuffle_and_encrypt = 1;
        break;
    case ARGP_KEY_ARG:
        if (state->arg_num >= 1)
            argp_usage (state);
        arguments->args[state->arg_num] = arg;
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };


/* MINE */


void print_array(char* array, size_t len){
    printf("\n{");
    for(int i=0; i < len; i++){
        printf(i != len -1? "%d,": "%d" , array[i]);
    }
    printf("}\n");
}

void print_array_l(long long* array, size_t len){
    printf("\n{");
    for(int i=0; i < len; i++){
        printf(i != len -1? "%lld,": "%lld" , array[i]);
    }
    printf("}\n");
}

int equals(int first, int second){
    int result;
    __asm__ volatile (
                "movl $0x0, %%ecx;"
            ".loop:"
                "movl %1, %%eax;"
                "movl %2, %%edx;"
                "shr %%cl, %%eax;"
                "and $0x1, %%eax;"
                "shr %%cl, %%edx;"
                "and $0x1, %%edx;"
                "cmp %%edx, %%eax;"
                "jnz .ne;"
                "inc %%ecx;"
                "cmpl $0x8, %%ecx;"
                "jnz .loop;"
                "movl $0x1, %%eax;"
                "jmp .end;"
            ".ne:"
                "movl $0x0, %%eax;"
            ".end:"
                "movl %%eax, %0;"
                : "=r"(result)
                : "r"(first), "r"(second)
                : "%eax", "%edx", "%ecx"
                );
    return result; // could leave it in eax
}

typedef struct Element{
    int decoded;
    int val;
    struct Element *next;
    struct Element *min;
} el;

el * find_min(el * list){
    el * min_element=NULL;
    if(list == NULL || list->next == NULL) {
        return list;
    }

    min_element = list;

    while(list != NULL){
        if(list->val < min_element->val){
            min_element = list;
        }
        list = list->next;
    }
    return min_element;

}


el* find_by_value(el *list, int value){
    el* curr_el = list;
    while(curr_el != NULL){
        if(equals(curr_el->val, value)){
            return curr_el;
        }
        curr_el = curr_el->next;
    }
    return NULL;
}


void del_by_value(el* list, int value){
    el * prev = NULL;
    while(list != NULL){
        if(equals(list->val, value)){
            el* to_free = list;
            if (prev !=  NULL){
                prev->next = to_free->next;
            }
            memset(to_free, 0, sizeof(el));
            free(to_free);
            return;
        }
        prev = list;
        list = list->next;
    }
    return;
}

el* create(int value){
    el* element = malloc(sizeof(el));
    memset(element, 0, sizeof(el));
    element->val = value;
    element->next = NULL;
    element->min = NULL;
    return element;

}

el* push(el* head, el* element_to_add ){
    element_to_add->next = head;
    head = element_to_add;
    el* min_el = find_min(head);
    el* lista = head;
    while(lista != NULL){
        lista->min = min_el;
        lista = lista->next;
    }
    return head;
}

void print_all(el* list){
    el *curr_el = list;
    while(curr_el != NULL){
        printf("Current el: %d\n", curr_el->val);
        curr_el = curr_el->next;
    }
}

int len(el* list){
    int count = 0;
    if(list == NULL) {
        return count;
    }
    do{
        count ++;
        list = list->next;
    }while(list != NULL && list->next != NULL);

    return count;
}

keypair_t * generate(){
    keypair_t * keypair = (keypair_t *) malloc(sizeof(keypair_t));
    private_key_t * private = (private_key_t *) malloc(sizeof(private_key_t));
    public_key_t * public = (public_key_t *) malloc(sizeof(public_key_t));
    rsa_gen_keys(public, private, PRIME_SOURCE_FILE);
    // Give some hint :P
    printf("\nPrivate\nMod: %lld\nexp: %lld\n", private->modulus,
                                              private->exponent);
    printf("\nPublic\nMod: %lld\nexp: %lld\n", public->modulus,
                                             public->exponent);
    keypair->private = private;
    keypair->public = public;
    return keypair;
}

keypair_t * generate_from_primes(long long p, long long q){
    keypair_t * keypair = (keypair_t *) malloc(sizeof(keypair_t));
    private_key_t * private = (private_key_t *) malloc(sizeof(private_key_t));
    public_key_t * public = (public_key_t *) malloc(sizeof(public_key_t));
    rsa_gen_keys_from_primes(public, private, p, q);
    // Give some hint :P
    printf("\nPrivate\nMod: %lld\nexp: %lld\n", private->modulus,
                                              private->exponent);
    printf("\nPublic\nMod: %lld\nexp: %lld\n", public->modulus,
                                             public->exponent);
    keypair->private = private;
    keypair->public = public;
    return keypair;
}



char * generate_shuffle(int size){
    srand(time(0));
    char * array = malloc(sizeof(char) * size);
    for(int i=0; i< size; i ++){
        array[i] = i ;
    }

    for (int i = 0; i < size - 1; i++) {
          int j = i +  rand() / (RAND_MAX / (size - i) + 1);
          int temp = array[j];
          array[j] = array[i];
          array[i] = temp;
    }
    return array;
}

long long* shuffle_and_encrypt(char *buffer){
    size_t len = strlen(buffer);
    keypair_t * keypair = generate();
    char * shuffle = generate_shuffle(len);

#ifdef DEBUG
    printf("Shuffle:\n");
    print_array(shuffle, len);
#endif
    long long * enc = rsa_encrypt(shuffle, len, keypair->public);
#ifdef DEBUG
    printf("Encrypted: ");
    print_array_l(enc, len);
    char * message = rsa_decrypt(enc, len, keypair->private);
    printf("decrypted: ");
    print_array(message, len);

#endif
    // Free the world
    free(keypair->private);
    free(keypair->public);
    free(keypair);
    free(shuffle);
    return enc;
}

long long* generate_and_encrypt(char* buffer){
    size_t len = strlen(buffer);

#ifdef DEBUG
    printf("len is %d\n", len);
#endif

    keypair_t * keypair = generate();

    long long * enc = rsa_encrypt(buffer, len, keypair->public);
    if (enc == NULL){
        // HINT HINT HINT
        printf("Error in rsa_encrypt: unable to encrypt message ");
        return NULL;
    }
#ifdef DEBUG
    printf("Encrypted Message:\n");
    print_array_l(enc, len);
    char * message = rsa_decrypt(enc, len, keypair->private);
    if (message == NULL){
        printf("Error in rsa_decrypt: unable to decrypt message ");
        return NULL;
    }
    for (int i = 0; i < len; ++i){
        printf("%c", message[i]);
    }
    printf("\n");
#endif
    // Free the world
    free(keypair->private);
    free(keypair->public);
    free(keypair);
    return enc;
}



int main(int argvc, char** argv){
    struct arguments arguments;
    memset(&arguments, 0, sizeof(struct arguments));
    argp_parse (&argp, argvc, argv, 0, 0, &arguments);


#ifdef DEBUG
    printf("Generate:   %d\n", arguments.generate);
    printf("P generate: %d\n", arguments.generate_from_primes);
    printf("Shuffle:    %d\n", arguments.generate_shuffle);
    printf("ShuEnc :    %d\n", arguments.shuffle_and_encrypt);
    printf("Encrypt:    %d\n", arguments.encrypt);
    printf("Input: %s\n", arguments.input);
#endif
    /*
    primes 16741:36457
    Hardcoded key used to encrypt and to factorize
    Private
    Mod: 610326637
    exp: 94984193
    Public
    Mod: 610326637
    exp: 257
    */

    public_key_t public = {
        .modulus = 610326637,
        .exponent = 257
    };

    if(arguments.generate){
        keypair_t * keypair = generate();
        free(keypair);
        return 0;
    }

    if(arguments.encrypt && strlen(arguments.input)){
        long long * enc = generate_and_encrypt(arguments.input);
        if(enc != NULL){
            print_array_l(enc, strlen(arguments.input));
        }
        return 0;
    }

    if(arguments.generate_shuffle && strlen(arguments.input)){
        size_t len = strlen(arguments.input);
        char *array = generate_shuffle(len);
        print_array(array, len);
        free(array);
        return 0;
    }

    if(arguments.shuffle_and_encrypt && strlen(arguments.input)){
        size_t len = strlen(arguments.input);
        long long *array = shuffle_and_encrypt(arguments.input);

        print_array_l(array, len);
        free(array);
        return 0;
    }

    if(arguments.generate_from_primes && strlen(arguments.input)){
        long long p = 0;
        long long q = 0;

        char * token = strtok(arguments.input, ":");
        if(token == NULL){
            exit(-1);
        }
        p = atol(token);
    #ifdef DEBUG
        printf("p: %llu\n", p);
    #endif
        token = strtok(NULL, ":");
        if(token == NULL){
            exit(-1);
        }

        q = atol(token);
    #ifdef DEBUG
        printf("q: %llu\n", q);
    #endif

        generate_from_primes(p, q);

    }

    // TODO Stuff
    return 0;
}
