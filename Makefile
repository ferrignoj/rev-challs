CC := gcc
OPT := -std=c99 -O0 -m32 -lm

challs := $(wildcard */*.c)
bins := $(challs:%.c=%.bin)
strip := $(challs:%.c=%.strip)


.PHONY: clean all

all: msg build strip

msg:
	@echo "Challenges are: "
	@echo $(challs)
	@echo

build: $(bins)
	@echo
%.bin: %.c
	@echo "Building: $<"
	@$(CC) $(OPT) -o $@ $<

strip: $(strip)
%.strip: %.bin
	@echo "Stripping: $<"
	@strip --strip-unneeded -o $@ $<

clean:
	-rm -f $(bins)
	-rm -f $(strip)
