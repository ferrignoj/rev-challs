Questa e' una collezione di challenge per Noemi e Laura.
Vanno da challenge semplici dicentando sempre piu' difficili.

In ogni cartella trovate:
- il sorgente (non guardatelo se non alla fine)
- file .bin: il file della challenge con ancora i simboli
- file .strip: il file della challenge senza simboli

Provate a lavorare su i binari .strip!

Per ogni cartella c'e' un file readme con una breve descritione della chall, degli argomenti su cui verte e potete cercare e dei tool che potreste utilizzare

# Alcuni link utili
Una guida estremamente datata ma che da sicuramente un ottimo incipit
http://www.textfiles.com/piracy/CRACKING/howto1.txt

Ci sono poi le slide sviluppater per il croso di reversing su cui ho lavorato e c'e' una buona base di partenza per asm e tool vari
 
